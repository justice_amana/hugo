---
title: Who is Pacific Coast Moving LLC?
subtitle: Doing some research on Pacific Coast Moving and those affiliated
date: 2020-09-08
tags: ["research", "pacific coast moving"]
---

My negative experience with Pacific Coast Moving LLC led me to do some research into who they really are. And while they claim to have been in business "for over twenty years", their oldest [Google Review](https://www.google.com/search?source=hp&ei=DyZDX9fGOpbbtQaNyKu4Aw&q=pacific+coast+moving&oq=pacific+coast+moving) is from 8 months ago. Their website was [created in 2019](https://whois.domaintools.com/paccoastmoving.com).

I decided to do some more digging. I look up their articles of incorporation on the CA SOS website. Every LLC must file their articles of incorporation in the state they do business in. Luckily most states provide this information for free as public record. Pacific Coast Moving LLC filed this last year, under the name "Gary White". I believe this is the same "Gary" who uses the email signature "Gary Wallace" and possibly other psuedonyms.

The registered agent for Pacific Coast Moving LLC is a "George Harris". Stay tuned, this name comes up later.

The DOT website shows them having [18 formal complaints](https://ai.fmcsa.dot.gov/hhg/SearchDetails.asp?ads=&id=30369533&id2=32391184) filed against them in 2020. And if you look up their USDOT number (3219046) on the [DOT Licensing and Insurance public records site](https://li-public.fmcsa.dot.gov/LIVIEW/pkg_carrquery.prc_carrlist), you'll see that they do not have insurance on file. This means that if your goods were damaged and you try to file a claim with them, there is literally no insurance company to give you the payout. I assume no insurance company wants to cover them after all the claims filed against them in the last 12 months.

For customers who didn't pay in cash, they will give you the option of paying via bank transfer. However, the account name they send you is "Moving Relocation Systems LLC". I wondered why they were using a different company name so I looked into that. Apparently, this was the company these individuals operated under until they obtained an Out-of-Service order and can no longer operate across state lines. Oh, and Moving Relocation Systems also had [55 formal complaints](https://ai.fmcsa.dot.gov/hhg/SearchDetails.asp?ads=&id=31278941&id2=32500665) filed against them in 2019.

I also looked up the Articles of Organization for Moving Relocation Systems LLC. One was in Indiana, also registered to a George Harris, in December 2018. Another was in Florida, registered by a Teresa White. More research dictates this is Gary's mom. Sorry, Gary about [your dad](https://www.legacy.com/obituaries/sunsentinel/obituary.aspx?n=gary-brent-white&pid=190823630).

The name "Brenda Murphy" also appears on the documents from Florida.

Looking up the Articles of Incorporation in NJ I found another name, Jim "Systems" Horky. I googled this name and found a third company! "[Cali Relocation Systems](https://www.yelp.com/biz/cali-relocation-systems-commerce-3)". This company was also registered in Indiana under the registered agent George Harris.

Back to Gary for a moment. With his full legal name at hand, "Gary Brent White Jr." I found another interesting thing. Gary Jr. used to own the company Student Debt Doctor LLC, which the [FTC filed a permanent injunction against](https://www.ftc.gov/enforcement/cases-proceedings/172-3055/student-debt-doctor-llc).

> A complaint filed in Florida federal court accuses Fort Lauderdale-based Student Debt Doctor (SDD) and its owner, Gary Brent White, Jr., of collecting more than $7 million in illegal upfront fees from customers seeking student loan debt forgiveness.

[Read more here](https://consumerist.com/2017/10/13/student-loan-debt-relief-operations-allegedly-bilked-95m-from-borrowers/). Also, interestingly [Jim Horky](https://www.trustpilot.com/review/studentdebtdoctor.org?page=8) is mentioned in some online reviews. It sounds like Jim and Gary go way back!

I could go on and on looking for records connecting the co-conspirers of Pacific Coast Moving, but I'll stop here for now. Please contact me at pacificicoastmovingreviews (at) gmail (dot) com if you have additional information you'd like me to add.
