---
title: How to spot a fake review
subtitle: Identifying and flagging fake reviews
date: 2020-09-14
tags: ["fake reviews","pacific coast moving"]
draft: true
---


- similar writing style

- referencing people at company

- only 1 or 5 star reviews

- the ratio of male:female reviewers is higher than one would expect if they were real

- new accounts posting 5 star reviews

- accounts that only have 1 review posted

- fake reviews are more likely to have exclamatino points and simple langauge

- fake reviews have fewer grammatical mistakes
