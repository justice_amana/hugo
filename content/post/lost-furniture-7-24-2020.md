---
title: Lost furniture 7/24/2020
subtitle: Missing furniture, sentimental bookcase, and vintage guitar
date: 2020-10-13
tags: ["lost and found","lost items"]
---

On July 24th, 2020, Pacific Coast Moving picked up the following items that they did not deliver to the intended recipient. If you find these items, please contact me at pacificcoastmovingreviews@gmail.com

![Vintage Guitar](/lost-and-found/image0.jpeg)
![Nightstand](/lost-and-found/image1.jpeg)
![Bookshelf, folding table, chairs](/lost-and-found/image2.jpeg)
![Coffee table](/lost-and-found/image3.jpeg)
![Large area rug](/lost-and-found/image4.jpeg)
![Large area rug](/lost-and-found/image5.jpeg)
