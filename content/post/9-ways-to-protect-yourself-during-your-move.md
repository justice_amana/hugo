---
title: 9 Ways to Protect Yourself
subtitle: How to prevent theft and extortion during your move
date: 2020-09-10
tags: ["prevention", "protection", "pacific coast moving"]
---

It's probably best if you don't use Pacific Coast Moving for your move, but if you decide to do it anyway, here are some ways to protect yourself.

**1) Read [Your Rights and Responsibilties When You Move](https://www.fmcsa.dot.gov/sites/fmcsa.dot.gov/files/docs/Rights-and-Responsibilities-2013.pdf)**

The Department of Transportation (DOT) has a special divison that regulates and oversees the moving of household goods, called the FMCSA, or Federal Motor Carrier Safety Administration. They are responsible for helping protect consumers from moving scams. Make sure you read the pdf thoroughly so you know what to expect!

**2) Take photos and/or videos on moving day**

This will provide you with evidence of what you had in case you need to file an insurance claim or lawsuit later. It may also help deter Pacific Coast from unwrapping and damaging your goods during storage, transport and delivery.

**3) Placing a GPS tracker in your things**

Pac Coast Moving has a reputation for storing goods for much longer than their contract permit and also refusing to disclose where goods are located. Having a GPS tracker planted can help you track down your goods in an emergency.

**4) Follow the moving truck to the warehouse**

The address listed on Pac Coast's website is not the actual warehouse where they store their customer's belongings (people have gone there are looked for it). You may chose to follow the moving truck after pick up to see where your things are actually being stored. This can also come in hand if they store them for too long and refuse to disclose the location.

**5) Place deterrants in your belongings**

You can place things like foul smells, glitter, motion activated noise makers or lights in your belongings. You may also consider adding locks to your boxes. This can help deter theft.

**6) Set up a video stream to monitor your belongings**

This can help you procure evidence of theft or damage if you are unlucky to have that happen to you. Based on Google Reviews, roughly half of customers experience some sort of theft or damage. A video stream can also give you peace of mind that your items are being handled with care.

**7) Ask to see personal identification**

You can ask to see identification for anyone affiliated with Pacific Coast Moving, including the movers themselves. They are known to use psuedonyms and lie compulsively, so having a copy or photo of an ID can help ensure your goods are handled well.

**8) Tell the movers not to work until contract is signed**

According to federal law, movers should not move your belongings until a price has been agreed on and a contract signed. If they attempt to package and move any items before a contract is signed, tell them to stop. If they don't stop, take a video for evidence.

**9) Have a backup plan**

Shady companies like Pacific Coast often use a bait and switch tactic. They convince you to use their service by offering a low price, then later on they increase the price once it's difficult to back out. If you have a backup plan it'll make it much easier to say "no" when they give you a contract with a massive price increase.
