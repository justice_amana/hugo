---
title: About
subtitle: Why did I even make this site?
date: 2020-09-08
comments: false
---

As a customer of Pacific Coast Moving LLC, I feel it's my duty to warn others before they succumb to the same fate I did. I also want to encourage victims to come forward with information. Some victims are considering filing a lawsuit against Gary White & company, and your testimonial could be valuable.

I'd also like to provide a platform to share real reviews. As you may already know, many of the reviews Pacific Coast Moving has on their website and on Google are fake. This site will only have vetted reviews by legitimate customers.
